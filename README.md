# Coronarekening Algemene Rekenkamer

In deze GIT staat de code voor de Coronarekening van de Algemene Rekenkamer welke gepubliceerd is op: https://coronarekening.rekenkamer.nl.

Het kabinet heeft vanaf maart 2020 verschillende steunmaatregelen ingezet om de gevolgen van de coronacrisis te ondervangen. Met deze monitor brengen we in kaart welke maatregelen door het kabinet zijn getroffen, voor wie ze zijn bedoeld, door wie ze worden uitgevoerd en wat bekend is over de resultaten ervan. 

Het dashboard is onderdeel van de webpublicatie Coronarekening: https://www.rekenkamer.nl/onderwerpen/corona/coronarekening

## Opzet code

```
coronarekening
| global.R
| ui.R
| server.R
| README.md
├── 0_data
├── 1_functies
├── 2_opwerking
├── 3_analyses
├── 4_output
├── 5_achtergrond
└── www
```

Global.R wordt gebruikt om het dashboard te starten: hierin worden de benodigde packages geladen, hulpbestanden geladen en als laatste ui.R en server.R als shinyserver gestart. De gebruikte databestanden staan in 0_data; functies in *.R bestanden met de naam van de functie staan in 1_functies; databewerkingen die niet afhankelijk zijn van gebruikersinput staan in 2_opwerking en 3_analyses; 4_output en 5_achtergrond worden niet gebruikt in dit project. In de folder www staan de bestanden die voor het dashboard online beschikbaar zijn.

## Copyright
Het op deze site gepubliceerde materiaal wordt gedeeld onder de [European Union Public Licence](https://eupl.eu/) (EUPL-1.2).

## Vragen
Voor vragen of opmerkingen kunt u contact opnemen via voorlichting@rekenkamer.nl
