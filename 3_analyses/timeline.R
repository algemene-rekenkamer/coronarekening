################################################################################
# timeline.R                                                                   #
#                                                                              #
# Dataframes voor tijdlijn en de groepen in de tijdlijn                        #
################################################################################

# Template voor informatie blokken van  maatregelen in html tabel
# In de blokken staat informatie over de naam maatregel, hoogte, start- en eind-
# datum. Deze worden in de functie in een tabel geplaatst

func_maatregel <- function(maatregel,
                           bedrag,
                           startd,
                           eindd,
                           link){
  sprintf(
    '<table>
       <thead>
          <tr>
            <th>%s</th>
          </tr>
       </thead>
       <tbody style="text-align:left">
       		<tr><td>%s</td></tr>
          <tr><td>van %s tot %s</td></tr>
          <tr><td>%s</td></tr>
       </tbody>
    </table>',
    maatregel, bedrag, startd, eindd, link
  )
}

# Dataframe voor tijdlijn
df_timeline <- df_maatregelen %>%
  group_by(naam) %>%
  mutate(uitgaven = sum(uitgaven, na.rm = TRUE),
         verplichtingen = sum(verplichtingen, na.rm = TRUE),
         ontvangsten = sum(ontvangsten, na.rm = TRUE)) %>%
  ungroup() %>%
  mutate(id = 1:n(),
         # Als er geen startdatum bekend is (NA) gebruiken we datum_maatregel
         start = coalesce(start_datum, datum_maatregel),
         # Correctie als de einddatum voorbij het eindpunt van de tijdlijn is
         end = if_else(eind_datum > "2021-12-31",
                                    "2021-12-31" %>% as.POSIXct,
                                     eind_datum),
         # Mouse-over laat naam en doel maatregel zien
         title = paste0(ministerie,
                       ": ",
                       doel_maatregel),
         group = hoofdstuk,
         gr_content = naam_begroting,
         type = ifelse(!is.na(end),
                       "point",
                       "point"),
         link = str_extract(link, "[^|]+")) %>% # Gebruik eerste link
  filter(!is.na(start)) %>%
  distinct(naam,
           .keep_all = TRUE)  %>%
  # Create subgroups per hfst arranged by start date
  group_by(ministerie) %>%
  arrange(start) %>%
  mutate(subgroup_order = 1:n()) %>%
  ungroup %>%
  mutate(#Inhoud van de blokken in de tijdlijn mbv func_maatregelen
         content = c(func_maatregel(
                    # Regel 1: naam
                    naam,
                    # Regel 2: bedrag (als niet NA anders "nog niet bekend")
                    ifelse(!is.na(uitgaven) &
                            uitgaven != 0,
                            paste("€",
                                  format(uitgaven %>% func_miljoen(),
                                         big.mark = ".",
                                         decimal.mark = ",",
                                         digits = 0),
                                  "miljoen"),
                            "Bedrag nog niet bekend"),
                    # Regel 3: looptijd
                    format(start, '%d-%m-%y'),
                    ifelse(!is.na(end),
                           format(end, '%d-%m-%y'),
                           "nog onbekend"),
                    # Regel 4: link naar bron
                    paste('<a href = ',
                          shQuote(link),
                          'target="_blank"',
                          '>',
                          'Klik hier voor bron',
                          '</a>')
                    )
             )
         ) %>%
  select(id,
         title,
         content,
         start,
         end,
         uitgaven,
         group,
         gr_content,
         subgroup_order,
         type) %>%
  arrange(subgroup_order)


# Template voor labels groepen
# De groepslabels werden te lang: daarom in een tabel met een woord per regel
func_groep <- function(groep,
                       gr_content_1,
                       gr_content_2,
                       gr_content_3,
                       gr_content_4){
  sprintf(
    '<table style="width:50%%">
       <thead>
          <tr>
            <th>%s</th>
          </tr>
       </thead>
       <tbody style="text-align:left">
       		<tr><td>%s</td></tr>
          <tr><td>%s</td></tr>
          <tr><td>%s</td></tr>
          <tr><td>%s</td></tr>
       </tbody>
    </table>',
    groep, gr_content_1, gr_content_2, gr_content_3, gr_content_4
  )
}

# # Data frame met informatie voor groepen van tijdslijn
df_tlgroups <- df_timeline %>%
  mutate(group_lab = gr_content) %>%
  arrange(group_lab) %>%
  # Split woorden van groepslabel
  splitstackshape::cSplit(splitCols = "group_lab",
                          sep = " ",
                          direction = "wide",
                          type.convert = "character") %>%
  # Vervang missing door een lege string
  mutate(group_lab_2 = fct_explicit_na(group_lab_2, ""),
         group_lab_3 = fct_explicit_na(group_lab_3, ""),
         group_lab_4 = fct_explicit_na(group_lab_4, "")) %>%
  # Tabel met groepslabels in een woord per rij
  mutate(content = func_groep(group,
                              group_lab_1,
                              group_lab_2,
                              group_lab_3,
                              group_lab_4))  %>%
  group_by(group) %>%
  select(id = group,
         gr_content,
         content) %>%
  distinct(id, .keep_all = TRUE)

