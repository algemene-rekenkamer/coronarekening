################################################################################
# pull_data.R                                                                  #
#                                                                              #
# Loads all data used in the dashboard                                         #
################################################################################

# DATASETS----------------------------------------------------------------------
# Data is aangemaakt in 01_begroting_data

# File location
fl_path <- "0_data/"

# Get list of available rds files
ls_rds <- list.files(path = normalizePath(fl_path),
                     pattern = "^\\d+.*.rds$") %>%
          as.data.frame()

# Get most recent dates from filenames
ls_maatregel <- ls_rds %>%
  dplyr::arrange() %>%
  filter(str_detect(., "corona_maatregelen.rds")) %>%
  tail(1) %>%
  pull() %>%
  as.character()

# Read data maatregelen
df_maatregelen <-
  normalizePath(paste0(fl_path,
                       ls_maatregel)) %>%
  readRDS() %>%
  ungroup()


################################################################################

# Zet bedragen voor 2020 en 2021 onderelkaar in long format df
# Zet nr_maatregel in hoofdletters, dat doen we later ook bij de realisatie cijfers zodat de match beter gaat.
df_maatregelen %<>%
  pivot_longer(cols = ends_with(c("_uitgaven",
                                  "_verplichtingen",
                                  "_ontvangsten",
                                  "_realisatie")),
               names_sep = "_",
               names_to = c("jaar",
                            "type")
               )%>%
  pivot_wider(names_from = "type") %>%
  mutate(jaar = jaar %>%
                str_remove(., "j") %>%
                as.integer(),
         nr_maatregel = toupper(nr_maatregel)) %>%
  select(-opnemen)



# Read date stamp from log file
stmp_time <-
  normalizePath(paste0(fl_path,
                       "log_dataframes.rds")) %>%
  readRDS() %>%
  filter(time_stored == max(time_stored)) %>%
  pull(time_stored) %>%
  unique()

## Gestyleerde datum van laatste update
noti_date <- format(stmp_time, "%d-%m-%Y")

# SELECTIES VOOR DASHBOARD------------------------------------------------------

# Deze lijst komt nu ook uit df_maatregelen om zo alleen departementen met
# maatregelen mee te nemen.
# To check; doen we hier iets mee?
ls_hoofdstukken <- df_maatregelen %>%
  select(naam_begroting) %>%
  filter(!is.na(naam_begroting)) %>%
  arrange(naam_begroting) %>%
  unique() %>%
  pull()

# JV 2020 DATA------------------------------------------------------------------
# Data is aangemaakt in 02_realisatie_jaarverslag_data

# Get most recent dates from filenames
ls_jv2020 <- ls_rds %>%
  dplyr::arrange() %>%
  filter(str_detect(., "realisaties_jv2020.rds")) %>%
  tail(1) %>%
  pull() %>%
  as.character()

df_jv2020 <-
  normalizePath(paste0(fl_path,
                       ls_jv2020)) %>%
  readRDS() %>%
  ungroup()


# JV 2021 DATA------------------------------------------------------------------
# Data is aangemaakt in 02_realisatie_jaarverslag_data

# Get most recent dates from filenames
ls_jv2021 <- ls_rds %>%
  dplyr::arrange() %>%
  filter(str_detect(., "realisaties_jv2021.rds")) %>%
  tail(1) %>%
  pull() %>%
  as.character()

df_jv2021 <-
  normalizePath(paste0(fl_path,
                       ls_jv2021)) %>%
  readRDS() %>%
  ungroup()


# JV 2022 DATA------------------------------------------------------------------
# Data is aangemaakt in 02_realisatie_jaarverslag_data

# Get most recent dates from filenames
ls_jv2022 <- ls_rds %>%
  dplyr::arrange() %>%
  filter(str_detect(., "realisaties_jv2022.rds")) %>%
  tail(1) %>%
  pull() %>%
  as.character()

df_jv2022 <-
  normalizePath(paste0(fl_path,
                       ls_jv2022)) %>%
  readRDS() %>%
  ungroup()








# TEXT VOOR DASHBOARD-----------------------------------------------------------
text_grafieken <-
  read_excel(normalizePath(paste0(fl_path,
                                  "tekst_grafieken.xlsx")))

text_faqs <-
  read_excel(normalizePath(paste0(fl_path,
                                  "tekst_faqs.xlsx"))) %>%
  rename_all(tolower) %>%
  rownames_to_column(var = "id")


