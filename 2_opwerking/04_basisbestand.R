


# Libraries gebruikt in deze markdown
library(tidyverse)
library(magrittr)
library(splitstackshape)
library(tidylog)
library(here)
library(readxl)
library(janitor)
library(lubridate)
library(kableExtra)


# Versies en doel

#|Datum aangemaakt  |Coder |Beschrijving |
#  |---------|-----|--------------------------------------------------------------|



  ## dit zijn eigenlijk stappen waarvan ik vind dat ze bij de opwerking moeten gebeuren, dus daar zullen ze naartoe worden geplaatst.
  ## ik wil alleen kijken of het lukt en daarom doe ik het eerst even hier.

df_jv2020$jaar<-"2020"
df_jv2021$jaar<-"2021"
df_jv2022$jaar<-"2022"

realisatie <- rbind(df_jv2020, df_jv2021,df_jv2022) %>%
            mutate(nr_maatregel = toupper(nr_maatregel))

#View(realisatie)

# we hebben alleen de uitgaven en ontvangsten van de realisaties in het nieuwe bestand. We beschikken niet over de realisaties van de verplichtingen.
# Is er een manier om deze informatie wel ter beschikking te krijgen?
# Ook verplichtingen kunnen wel of niet gerealiseerd worden lijkt mij?!

realisatie$jaar<-as.integer(realisatie$jaar)


# Dit bestand moet worden gekoppeld aan het bestand met de begrote bedragen.
# De namen van alle maatregelen wijken alleen heel erg van elkaar af. Het zou goed zijn om dat op te lossen.
# Als we het kunnen koppelen wordt het namelijk ook wel echt een beter verhaal.

# Eerste koppeling
# Bestand df_maatregelen bestaat uit 837 cases.
# Bestand realisaties bestaat uit 245 cases.
# left join ipv full join. In realisatiebestand nemen we enkel mee wat we hebben gevonden in de begroting. De rest blijkt namelijk niet nuttig.



basisbestand <-  df_maatregelen %>%
  left_join(realisatie, by=c("nr_maatregel", "jaar") ) %>%
  rename(maatregel_begroting = naam.x  ,
         maatregel_jaarverslag = naam.y,
         uitgaven_begroot = uitgaven.x,
         ontvangsten_begroot = ontvangsten.x,
         verplichting_begroot = verplichtingen,
         uitgaven_realisatie = uitgaven.y,
         ontvangsten_realisatie = ontvangsten.y,
         verplichting_realisatie = verplichting) %>%
  filter(!is.na(uitgaven_begroot) | !is.na(verplichting_begroot) | !is.na(ontvangsten_begroot) |
           !is.na(uitgaven_realisatie)| !is.na(verplichting_realisatie) | !is.na(ontvangsten_realisatie))  %>%
  select(-realisatie)



# Als in een jaar niets is begroot of uitgegeven kan de regel weg.
# tot_basisbestand_orig <- tot_basisbestand

tot_basisbestand <- basisbestand %>%
  pivot_longer(cols = ends_with(c("_realisatie",
                                  "_begroot")),
               names_sep = "_",
               names_to = c("vuo",
                            "type")
  )          %>%
  pivot_wider(names_from = "vuo")

# behouden realisatie die we hebben en gooien voor die jaren de begrote cijfers weg. Dwz we houden de begrote cijfers voor  2023.
# @ Lisanne, ik twijfel of deze code helemaal goed is.
tot_basisbestand <- tot_basisbestand %>%
                    filter((type == "begroot" & jaar %in% c("2023")) | type == "realisatie" ) %>%
                    mutate(jaar_label = ifelse(jaar %in% c("2023"),paste0(jaar," (begroot)"),jaar))

tot_basisbestand$jaar_label <- factor(tot_basisbestand$jaar_label, levels = c("2020", "2021", "2022", "2023 (begroot)"))

# voor grafieken op rijksbreed niveau willen we een clustering aanbrengen. Dat doen we hieronder.
  tot_basisbestand$doelgroep_cluster <- fct_recode(tot_basisbestand$doelgroep_beperkt,
                                         "Bedrijven" = "Agrarische sector",
                                         "Overig" = "Andere landen",
                                         "Overig" = "Burgers",
                                         "Medeoverheden" = "Caribisch Nederland",
                                         "Medeoverheden" = "Caribische landen",
                                         "Overig" = "Culturele sector",
                                         "Overig" = "EU",
                                         "Medeoverheden" = "Gemeenten",
                                         "Overig" = "Internationale organisaties",
                                         "Overig" = "Jongeren",
                                         "Bedrijven" = "Luchtvaart",
                                         "Overig" = "Media",
                                         "Overig" = "Ministerie van EZK en InvestNL",
                                         "Bedrijven" = "MKB/ZZP",
                                         "Overig" = "Nieuwkomers",
                                         "Bedrijven" = "Ondernemers",
                                         #"Overig" = "Onderwijs",
                                         #"Overig" = "Onderzoek",
                                         "Bedrijven" = "Openbaar vervoer",
                                         "Overig" = "Ouders",
                                         "Overig" = "Recreatie",
                                         "Overig" = "Sport",
                                         "Bedrijven" = "Startende ondernemers",
                                         "Bedrijven" = "Starters",
                                         "Bedrijven" = "Startups",
                                         "Zorg" = "Testcapaciteit",
                                         "Bedrijven" = "Transport",
                                         "Overig" = "Uitvoering",
                                         "Zorg" = "Vaccins",
                                         "Bedrijven" = "Werkgevers",
                                         "Bedrijven" = "Werknemers",
                                         "Bedrijven" = "Zelfstandige ondernemers"
  )
#eenmalig aanmaken tbv webpublicatie
 # saveRDS(tot_basisbestand,"tot_basisbestand_21_4_2023.Rds")
